/* https://www.jenkins.io/doc/book/pipeline */
pipeline {

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#agent */
  agent {
    label 'linux'
  }

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#triggers */
  triggers { 
    pollSCM('H/5 * * * *')
  }

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#options */
  options {
    buildDiscarder(
      logRotator(
        numToKeepStr: '10'
      )
    )

    timeout(
      time: 1,
      unit: 'HOURS'
    )
  } /* options */

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#stages */
  stages {
    stage('Setup') {
      steps {
        buildName '#' + env.BUILD_NUMBER + ' - ' + env.GIT_BRANCH + ' - ' + env.GIT_COMMIT.take(7)
      }
    }

    stage('Builds') {
      /* https://www.jenkins.io/doc/book/pipeline/syntax/#declarative-matrix */
      matrix {
        axes {
          axis {
            name 'BUILD_ARCH'
            values 'M32=1', 'M64=1'
          }
          axis {
            name 'BUILD_DEBUG'
            values 'DEBUG=0', 'DEBUG=1'
          }
        }

        /* https://www.jenkins.io/doc/book/pipeline/syntax/#stages */
        stages {
          stage('Build') {
            steps {
              sh 'make DESTDIR="dist" $BUILD_ARCH $BUILD_DEBUG VERBOSE=1 all samples install'
            }
          }
        } /* stages */
      } /* matrix */
    }

    stage('Documentation') {
      steps {
        sh 'make VERBOSE=1 doc doc-pdf'
      }
    }
  } /* stages */

  /* https://www.jenkins.io/doc/book/pipeline/syntax/#post */
  post {
    always {
      /* https://www.jenkins.io/doc/pipeline/steps/chucknorris */
      chuckNorris()

      /* https://www.jenkins.io/doc/pipeline/steps/core */
      archiveArtifacts(
        artifacts: 'dist/**, doc/html/**, doc/man/**, doc/*.pdf, build/samples/**',
        allowEmptyArchive: true,
        defaultExcludes: true,
        caseSensitive: true
        /* 'follow symbolic links' can't be set yet */
      )

      /* https://www.jenkins.io/doc/pipeline/steps/mailer */
      step(
        [
          $class: 'Mailer',
          recipients: env.BUILD_NOTIFIER_EMAIL_ADDRESS,
          notifyEveryUnstableBuild: true
        ]
      )
    } /* always */
  } /* post */

} /* pipeline */

