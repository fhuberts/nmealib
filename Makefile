include Makefile.inc

#
# Settings
#

DESTDIR ?=
USRDIR ?= $(DESTDIR)/$(DISTARCH)/$(DISTDEBUG)/usr
INCLUDEDIR ?= $(DESTDIR)/$(DISTARCH)/$(DISTDEBUG)/usr/include
LIBDIR ?= $(USRDIR)/lib

H_FILES = $(wildcard include/nmealib/*.h)
C_FILES = $(wildcard src/*.c)

MODULES = $(C_FILES:src/%.c=%)

BUILD_DIR=build/$(DISTARCH)/$(DISTDEBUG)
BUILD_LIB_DIR=$(BUILD_DIR)/lib

OBJ = $(MODULES:%=$(BUILD_DIR)/%.o)

LIBRARIES = -lm
INCLUDES = -I ./include


.PRECIOUS: $(OBJ)


#
# Targets
#

all: default_target

default_target: all-before $(BUILD_LIB_DIR)/$(LIBNAMESTATIC) $(BUILD_LIB_DIR)/$(LIBNAME)

remake: clean all

$(BUILD_LIB_DIR)/$(LIBNAMESTATIC): $(OBJ)
ifeq ($(VERBOSE),0)
	@echo "[AR] $@"
endif
	$(MAKECMDPREFIX)ar rcs "$@" $(OBJ)

$(BUILD_LIB_DIR)/$(LIBNAME): $(OBJ)
ifeq ($(VERBOSE),0)
	@echo "[LD] $@"
endif
	$(MAKECMDPREFIX)$(CC) $(LDFLAGS) -Wl,-soname=$(LIBNAME) -o "$@" $(LIBRARIES) $(OBJ)

$(BUILD_DIR)/%.o: src/%.c $(H_FILES) Makefile Makefile.inc
ifeq ($(VERBOSE),0)
	@echo "[CC] $<"
endif
	$(MAKECMDPREFIX)$(CC) $(CFLAGS) $(INCLUDES) -c $< -o $@

full : all samples test

test: all
	$(MAKECMDPREFIX)$(MAKE) -C test all

samples: all
	$(MAKECMDPREFIX)$(MAKE) -C samples all

check: test samples
	$(MAKECMDPREFIX)$(MAKE) -C test check


#
# Phony Targets
#

.PHONY: all-before clean doc doc-pdf doc-all doc-clean install install-headers uninstall uninstall-headers

all-before:
	$(MAKECMDPREFIX)mkdir -p $(BUILD_DIR) $(BUILD_LIB_DIR)

clean:
	$(MAKECMDPREFIX)$(MAKE) -C test clean
	$(MAKECMDPREFIX)$(MAKE) -C samples clean
	$(MAKECMDPREFIX)$(MAKE) -C doc clean
	$(MAKECMDPREFIX)rm -fr $(BUILD_DIR) $(BUILD_LIB_DIR)

doc:
	$(MAKECMDPREFIX)$(MAKE) -C doc doc

doc-pdf:
	$(MAKECMDPREFIX)$(MAKE) -C doc doc-pdf

doc-all:
	$(MAKECMDPREFIX)$(MAKE) -C doc all

doc-clean:
	$(MAKECMDPREFIX)$(MAKE) -C doc clean

install: all
	$(MAKECMDPREFIX)mkdir -v -p "$(LIBDIR)"
	$(MAKECMDPREFIX)cp -v "$(BUILD_LIB_DIR)/$(LIBNAME)" "$(LIBDIR)/$(LIBNAME).$(VERSION)"
	$(MAKECMDPREFIX)$(STRIP) "$(LIBDIR)/$(LIBNAME).$(VERSION)"
	$(MAKECMDPREFIX)/sbin/ldconfig -n "$(LIBDIR)"

install-headers: all
	$(MAKECMDPREFIX)mkdir -v -p "$(INCLUDEDIR)"
	$(MAKECMDPREFIX)rm -fr "$(INCLUDEDIR)/nmealib"
	$(MAKECMDPREFIX)cp -rv include/nmealib "$(INCLUDEDIR)"

uninstall:
	$(MAKECMDPREFIX)rm -fv "$(LIBDIR)/$(LIBNAME)" "$(LIBDIR)/$(LIBNAME).$(VERSION)"
	$(MAKECMDPREFIX)/sbin/ldconfig -n "$(LIBDIR)"
	$(MAKECMDPREFIX)rmdir -v -p --ignore-fail-on-non-empty "$(LIBDIR)"

uninstall-headers:
	$(MAKECMDPREFIX)rm -frv "$(INCLUDEDIR)/nmealib"
	$(MAKECMDPREFIX)rmdir -v -p --ignore-fail-on-non-empty "$(INCLUDEDIR)"

